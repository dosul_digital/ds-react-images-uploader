/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import { useField } from '@unform/core';
import InputLabel from '@material-ui/core/InputLabel';
import ImageUpload from '../CustomInput/ImageUpload';
import ImagemPrincipal, {
  ImagemPrincipalInput,
} from '../CustomInput/ImageUpload/ImagemPrincipal';

export default function ImagePicker({
  title,
  name,
  path,
  form,
  idField,
  imagemPrincipal,
  afterAction,
  ...rest
}) {
  const [update, setUpdate] = React.useState(false);

  React.useEffect(() => {
    if (update) {
      setUpdate(!update);
    }
  }, [update]);

  const inputRef = React.useRef(null);
  const { fieldName, registerField, error, defaultValue } = useField(name);

  React.useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
      getValue(ref) {
        try {
          if (ref.value) {
            return JSON.parse(ref.value);
          }
          return null;
        } catch (e) {
          return null;
        }
      },
      clearValue(ref) {
        ref.value = null;
      },
      setValue(ref, value) {
        ref.value = JSON.stringify(value);
      },
    });
  }, [fieldName, registerField]);

  return (
    <div>
        <InputLabel style={{ paddingTop: '15px', paddingBottom: '5px' }}>
          {title || 'Imagens'}
        </InputLabel>
        {error && <p>{error}</p>}
        <ImagemPrincipalInput />
        <input
          style={{ display: 'none' }}
          ref={inputRef}
          type="text"
          defaultValue={defaultValue && JSON.stringify(defaultValue)}
        />
        <ImageUpload
          extraData={{ path, fieldName: 'imagens' }}
          dataName="imagens[]"
          multiple
          debug
          label={' '}
          parent={imagemPrincipal ? ImagemPrincipal : null}
          images={(form?.current?.getFieldValue(name) || []).map(
            (val) => val.delete !== true ? val.url : ''
          )}
          parentprops={{
            form,
            setUpdate,
            afterAction,
          }}
          classNames={{
            container: 'iu-container left',
          }}
          deleteImage={(key) => {
            const tmpImagens = form?.current
              ?.getFieldValue(name)
              .map((val, idx) => {
                if (idx === key) {
                  return {
                    ...val,
                    delete: true,
                  };
                }
                return val;
              });
            form?.current?.setFieldValue(name, tmpImagens);
            if (afterAction) {
              afterAction(tmpImagens);
            }
			console.log('Imagens form', form?.current?.getFieldValue('Imagens'));
          }}
          afterLoad={(response) => {
            const oldImagens = form?.current?.getFieldValue(name) || [];
            for (let i = 0; i < response.length; i++) {
              oldImagens.push(response[i]);
            }
            form?.current?.setFieldValue(name, oldImagens);

            if (afterAction) {
              afterAction(oldImagens);
            }
			console.log('Imagens form', form?.current?.getFieldValue('Imagens'));
          }}
          {...rest}
        />
    </div>
  );
}

ImagePicker.propTypes = {
  title: PropTypes.string,
  name: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
  form: PropTypes.any.isRequired,
  imagemPrincipal: PropTypes.bool,
  idField: PropTypes.string.isRequired,
  afterAction: PropTypes.func,
};

ImagePicker.defaultProps = {
  title: undefined,
  afterAction: undefined,
  imagemPrincipal: false,
};
