/* eslint-disable */
/* Geral / Bibliotecas */
import React, { useState, useEffect, useRef, useCallback } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { Form } from '@unform/web';
import dynamic from 'next/dynamic';

const HelpButton = styled.button.attrs({
  color: 'primary',
  justIcon: true,
  round: true,
  title: 'Ajuda',
})`
  position: fixed !important;
  top: 5px;
  right: 10px;
  z-index: 9999;
`;

export default function DefaultForm({
  view,
  idField,
  id,
  model,
  preSave = (values) => values,
  afterGet = (values) => values,
  afterSuccess = () => {},
  afterError = () => {},
  // eslint-disable-next-line no-unused-vars
  children = () => {
    return (
      <div>Função não disponível, entre em contato para mais informações.</div>
    );
  },
  hiddenIdField,
}) {
  const disabled = false;

  const [refreshLayout, setRefreshLayout] = React.useState(false);
  const formRef = useRef(null);
  const [currentModel, setCurrentModel] = useState(model);
  const [isSubmitting, setIsSubmitting] = useState(false);

  React.useEffect(() => {
    async function check() {
      if (refreshLayout) {
        setRefreshLayout(false);
      }
    }

    check();
  }, [refreshLayout]);

  return (
	<Form ref={formRef} initialData={currentModel}>
		<>
			<div style={{ width: '100%', marginTop: '20px' }}>
			{children(formRef, disabled, {
				data: currentModel,
			})}
			</div>
		</>
	</Form>
  );
}

DefaultForm.propTypes = {
  view: PropTypes.string.isRequired,
  idField: PropTypes.string,
  id: PropTypes.any,
  model: PropTypes.object.isRequired,
  hiddenIdField: PropTypes.bool,
};

DefaultForm.defaultProps = {
  id: undefined,
  idField: undefined,
  hiddenIdField: true,
};
