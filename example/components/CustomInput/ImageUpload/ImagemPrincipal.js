/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import { useField } from '@unform/core';
import Star from '@material-ui/icons/Star';
import StarBorder from '@material-ui/icons/StarBorder';

export function ImagemPrincipalInput() {
  const inputRef = React.useRef(null);
  const { fieldName, registerField, defaultValue, error } = useField(
    'ImagemPrincipal'
  );

  React.useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
      getValue(ref) {
        try {
          if (ref.value) {
            return JSON.parse(ref.value);
          }
          return null;
        } catch (e) {
          return null;
        }
      },
      clearValue(ref) {
        ref.value = null;
      },
      setValue(ref, value) {
        ref.value = JSON.stringify(value);
      },
    });
  }, [fieldName, registerField]);

  return (
    <>
      <input
        style={{ display: 'none' }}
        ref={inputRef}
        type="text"
        defaultValue={defaultValue && JSON.stringify(defaultValue)}
      />
      {error && <p>{error}</p>}
    </>
  );
}

export default function ImagemPrincipal({
  children,
  form,
  setUpdate,
  afterAction,
}) {
  function isMain(fmk, key) {
    const imagemPrincipal = fmk?.current?.getFieldValue('ImagemPrincipal');
    return (
      imagemPrincipal !== null &&
      imagemPrincipal.name &&
      fmk?.current?.getFieldValue('Imagens') &&
      fmk?.current?.getFieldValue('Imagens')[key] &&
      fmk?.current?.getFieldValue('Imagens')[key].name &&
      imagemPrincipal.name === fmk.current.getFieldValue('Imagens')[key].name
    );
  }

  const main = isMain(form, parseInt(children.key, 10));

  return (
        <div style={{ width: 'auto', display: 'inline-block' }}>
          {children}
          <div
            style={{
              position: 'absolute',
              zIndex: 99,
              transform: 'translateY(calc(-100% - 6px))',
              background: 'green',
              height: '26px',
              width: '26px',
              borderBottomLeftRadius: '10px',
              borderTopRightRadius: '10px',
              border: `1px solid #000`,
              transition: 'all 0.3s ease',
            }}
          >
            {main && <Star title="Principal" style={{ color: 'white' }} />}
            {!main && (
              <StarBorder
                onClick={() => {
                  form.current.setFieldValue(
                    'ImagemPrincipal',
                    form.current.getFieldValue('Imagens')[
                      parseInt(children.key, 10)
                    ]
                  );
                  setUpdate(true);
                  if (afterAction) {
                    afterAction(true);
                  }
                }}
                style={{ color: 'white' }}
              />
            )}
          </div>
        </div>
  );
}

ImagemPrincipal.propTypes = {
  form: PropTypes.any.isRequired,
  setUpdate: PropTypes.func.isRequired,
  children: PropTypes.any.isRequired,
  afterAction: PropTypes.func,
};

ImagemPrincipal.defaultProps = {
  afterAction: undefined,
};
