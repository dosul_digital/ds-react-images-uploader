/* eslint-disable */
import React from 'react';
import PropTypes from 'prop-types';
import { useField } from '@unform/core';
import InputLabel from '@material-ui/core/InputLabel';
import ImageUpload from '../CustomInput/ImageUpload';

export default function ImagePicker({
  title,
  name,
  path,
  form,
  idField,
  preview,
  ...rest
}) {
  const inputRef = React.useRef(null);
  const { fieldName, registerField, defaultValue } = useField(name);
  const { error } = useField('Imagem.name');

  React.useEffect(() => {
    registerField({
      name: fieldName,
      ref: inputRef.current,
      path: 'value',
      getValue(ref) {
        try {
          if (ref.value) {
            return JSON.parse(ref.value);
          }
          return null;
        } catch (e) {
          return null;
        }
      },
      clearValue(ref) {
        ref.value = null;
      },
      setValue(ref, value) {
        ref.value = JSON.stringify(value);
      },
    });
  }, [fieldName, registerField]);

  return (
    <>
		<input
		style={{ display: 'none' }}
		ref={inputRef}
		type="text"
		defaultValue={defaultValue && JSON.stringify(defaultValue)}
		/>
		<InputLabel style={{ paddingTop: '15px', paddingBottom: '5px' }}>
		{title || 'Imagem'}
		</InputLabel>
		{error && <HelperTextError error>{error}</HelperTextError>}
		<ImageUpload
		extraData={{ path, fieldName: 'imagem' }}
		dataName="imagem"
		multiple={false}
		label={' '}
		preview={preview}
		parentprops={{
			form,
		}}
		classNames={{
			container: 'iu-container left',
		}}
		image={
			form?.current?.getFieldValue(name)
			? form?.current?.getFieldValue(name).url
			: defaultValue?.url || null
		}
		afterLoad={(response) => {
			form?.current?.setFieldValue(name, response);
		}}
		deleteImage={() => {
			const baseId = btoa(form?.current?.getFieldValue(idField));
			const filename = form?.current?.getFieldValue(name);

			axios.delete('media', {
			data: {
				path,
				filename: filename.name,
			},
			});
			axios.delete(`${path}/delete-media/${baseId}`);

			form?.current?.setFieldValue(name, null);
		}}
		{...rest}
		/>
    </>
  );
}

ImagePicker.propTypes = {
  title: PropTypes.string,
  name: PropTypes.string.isRequired,
  path: PropTypes.string.isRequired,
  form: PropTypes.any.isRequired,
  idField: PropTypes.string.isRequired,
  preview: PropTypes.bool,
};

ImagePicker.defaultProps = {
  title: null,
  preview: false,
};
