/* eslint-disable */
const withCSS = require('@zeit/next-css');

module.exports = withCSS({
  trailingSlash: true,
  reactStrictMode: false,
  distDir: process.env.NODE_ENV === 'production' ? './.next' : './.next_dev',
});
