import React from 'react';

// Components
import DefaultForm from '../components/DefaultForm';
import MultipleImagePicker from '../components/MultipleImagePicker';
import ImagePicker from '../components/ImagePicker';

export default function Example() {
	return (
		<div>
			<DefaultForm
				view={'example'}
				model={{
					Imagens: [

					],
				}}>
				{(form, disabled, { refresh, data }) => (
					process.browser ? (
						<>
							<MultipleImagePicker
								form={form}
								disabled={disabled}
								title="MultipleImagePicker"
								name="Imagens"
								path="produtos"
								imagemPrincipal
								afterAction={() => {}}
							/>
							<ImagePicker
								form={form}
								disabled={disabled}
								title="ImagePicker"
								name="Imagem"
								path="produtos"
								afterAction={() => {}}
							/>
						</>
					) : (
						<div>Carregando...</div>
					)
        )}
			</DefaultForm>
		</div>
	);
}
